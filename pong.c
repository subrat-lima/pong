#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <ncurses.h>

struct player {
  size_t x;
  size_t y;
  size_t score;
};

struct ball {
  size_t x;
  size_t y;
  bool ver;
  bool hor;
};

struct screen {
  size_t x;
  size_t y;
};

void show_intro() {
  char *str = "\t                               \n"
              "\t ____                          \n"
              "\t|  _ \\ ___  _ __   __ _       \n"
              "\t| |_) / _ \\| '_ \\ / _` |     \n"
              "\t|  __/ (_) | | | | (_| |       \n"
              "\t|_|   \\___/|_| |_|\\__, |     \n"
              "\t                  |___/      \n\n"
              "\tPress Any Key to start         \n"
              "\tControls                       \n"
              "\t--------                       \n"
              "\tPlayer 1:                      \n"
              "\t         q, a                  \n"
              "\tPlayer 2:                      \n"
              "\t         UP, DOWN            \n\n"
              "\tp to PAUSE                     \n"
              "\tESC to QUIT                  \n\n"
              "\tFirst to score 5 point wins.   \n";
  mvprintw(1, 1, str);
  getch();
}

void init_player(struct player *p, size_t y, size_t x) {
  p->y = y;
  p->x = x;
  p->score = 0;
}

void init_ball(struct ball *b, size_t y, size_t x) {
  b->y = y;
  b->x = x;
  b->ver = true;
  b->hor = true;
}

void init_screen(struct screen *s) {
  getmaxyx(stdscr, s->y, s->x);
}

void init_game(struct player *p1, struct player *p2, struct ball *b, struct screen *s) {
  init_screen(s);
  init_player(p1, s->y/2, 1);
  init_player(p2, s->y/2, s->x-2);
  init_ball(b, s->y/2, s->x/2);
}

void draw_player(struct player *p) {
  for(int i=-1; i<2; i++)
    mvaddch(p->y+i, p->x, '|');
}

void draw_ball(struct ball *b) {
  mvaddch(b->y, b->x, 'o');
}

void draw_score(struct player *p1, struct player *p2) {
  mvprintw(1, p2->x/2 - 3, "%2zu", p1->score);
  mvprintw(1, p2->x/2 + 3, "%2zu", p2->score);
  mvvline(0, p2->x/2 + 1, '|', 100);
}

void check_screen_resize(struct player *p1, struct player *p2, struct ball *b, struct screen *s) {
  size_t y, x;
  getmaxyx(stdscr, y, x);
  if(s->x != x || s->y != y) {
    s->x = x;
    s->y = y;
    p1->y = y/2;
    p1->x = 1;
    p2->y = y/2;
    p2->x = x-2;
    b->x = x/2;
    b->y = y/2;
  }
}

void draw_screen(struct player *p1, struct player *p2, struct ball *b, struct screen *s) {
  erase();
  check_screen_resize(p1, p2, b, s);
  draw_score(p1, p2);
  attron(COLOR_PAIR(1));
  draw_player(p1);
  draw_player(p2);
  draw_ball(b);
  attroff(COLOR_PAIR(1));
}

void check_player(struct player *p, size_t y) {
  if(p->y < 1)
    p->y = 1;
  if(p->y > y - 2)
    p->y = y - 2;
}

void check_ball_player(struct player *p, struct ball *b) {
  if(b->x + 1 == p->x || b->x - 1 == p->x) {
    if(b->y >= p->y - 1 && b->y <= p->y + 1)
      b->hor = !b->hor;
  }
}

void check_ball_boundary(struct player *p1, struct player *p2, struct ball *b, size_t y, size_t x) {
  if(b->x < 1 || b->x > x - 2) {
    if(b->x < 1)
      p2->score++;
    else
      p1->score++;
    b->x = x/2;
    b->y = y/2;
  }
  if(b->y < 1 || b->y > y - 2)
    b->ver = !b->ver;
}

void update_ball(struct ball *b) {
  b->x = b->hor ? ++b->x : --b->x;
  b->y = b->ver ? ++b->y : --b->y;
}

bool reached_score(struct player *p) {
  if(p->score == 1)
    return true;
  return false;
}

void show_winner(struct screen *s, char *str, bool *loop) {
  nodelay(stdscr, 0);
  attron(COLOR_PAIR(2));
  mvprintw(s->y/2, s->x/2 - 12, " Player %s is the winner ", str);
  attroff(COLOR_PAIR(2));
  getch();
  *loop = false;
}

void game_loop(struct player *p1, struct player *p2, struct ball *b, struct screen *s) {
  nodelay(stdscr, 1);
  bool loop = true;
  size_t timer = 0;
  while(loop) {
    switch(getch()) {
      case KEY_DOWN:
        p2->y++;
        break;
      case KEY_UP:
        p2->y--;
        break;
      case 'a':
        p1->y++;
        break;
      case 'q':
        p1->y--;
        break;
      case 'p':
        getchar();
        break;
      case 0X1B:
        loop = !loop;
        break;
    }
    timer++;
    check_player(p1, s->y);
    check_player(p2, s->y);
    check_ball_player(p1, b);
    check_ball_player(p2, b);
    check_ball_boundary(p1, p2, b, s->y, s->x);
    if(timer %  7 == 0) {
      update_ball(b);
      timer = 0;
    }
    draw_screen(p1, p2, b, s);
    usleep(5000);
    if(p1->score == 5) {
      show_winner(s, "1", &loop);
    }
    if(p2->score == 5) {
      show_winner(s, "2", &loop);
    }
  }
}

void start_game() {
  struct player p1, p2;
  struct ball b;
  struct screen s;

  erase();
  show_intro();
  init_game(&p1, &p2, &b, &s);
  draw_screen(&p1, &p2, &b, &s);
  game_loop(&p1, &p2, &b, &s);
}

void quit() {
  endwin();
}

void init_ncurses() {
  initscr();
  atexit(quit);
  noecho();
  curs_set(0);
  keypad(stdscr, true);
  start_color();
  init_pair(1, COLOR_BLUE, COLOR_BLACK);
  init_pair(2, COLOR_BLUE, COLOR_WHITE);
}

int main() {
  init_ncurses();
  start_game();

  return EXIT_SUCCESS;
}
